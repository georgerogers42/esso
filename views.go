package esso

import (
	"github.com/gorilla/mux"
	"html/template"
	"net/http"
)

var BaseTpl = template.Must(template.ParseFiles("views/base.tpl"))

var IndexTpl = template.Must(template.Must(BaseTpl.Clone()).ParseFiles("views/index.tpl"))

func Index(w http.ResponseWriter, r *http.Request) {
	Lock.RLock()
	defer Lock.RUnlock()
	IndexTpl.Execute(w, ArticlesList)
}

var ArticleTpl = template.Must(template.Must(BaseTpl.Clone()).ParseFiles("views/article.tpl"))

func ViewArticle(w http.ResponseWriter, r *http.Request) {
	Lock.RLock()
	defer Lock.RUnlock()
	title := mux.Vars(r)["title"]
	if article, ok := Articles[title]; ok {
		ArticleTpl.Execute(w, article)
	} else {
		http.NotFound(w, r)
	}
}
