package main

import (
	"bitbucket.org/georgerogers42/esso"
	"fmt"
	"log"
	"net/http"
	"os"
	"runtime"
)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())
	http.Handle("/static/", http.FileServer(http.Dir("public")))
	http.Handle("/", esso.App)
	port := os.ExpandEnv(":$PORT")
	fmt.Println("Listening on:", port)
	log.Fatal(http.ListenAndServe(port, nil))
}
