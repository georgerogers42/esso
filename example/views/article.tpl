{{define "head"}}
<link rel="stylesheet" href="/static/style.css">
<title>Essocony&mdash;{{.Title}}</title>
{{end}}
{{define "body"}}
<div class="title">
	<h1 class="title"><a href="/">Essocony</a></h1>
</div>
<div class="article">
	<h1 class="article-title"><a href="/{{.Slug}}">{{.Title}}</a></h1>
	<h2 class="date">{{.Date}}</h2>
	<div class="text">{{.Text}}</div>
</div>
{{end}}
