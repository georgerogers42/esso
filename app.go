package esso

import (
	"github.com/gorilla/mux"
	"path/filepath"
	"sort"
)

var App = mux.NewRouter()

func LoadArticles() {
	Lock.Lock()
	defer Lock.Unlock()
	Articles = map[string]*Article{}
	articles, err := filepath.Glob("articles/*.markdown")
	if err != nil {
		panic(err)
	}
	for _, article := range articles {
		a, err := LoadArticle(article)
		if err != nil {
			panic(err)
		}
		Articles[a.Slug] = a
	}
	ArticlesList = make(ArticleList, 0, len(Articles))
	for _, article := range Articles {
		ArticlesList = append(ArticlesList, article)
	}
	sort.Sort(ArticlesList)
}

func init() {
	LoadArticles()
	App.HandleFunc("/", Index)
	App.HandleFunc("/article/{title}", ViewArticle)
}
